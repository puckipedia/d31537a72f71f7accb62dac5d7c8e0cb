### Keybase proof

I hereby claim:

  * I am puckipedia on github.
  * I am puckipedia (https://keybase.io/puckipedia) on keybase.
  * I have a public key whose fingerprint is 75CF 6C8F 8349 6503 6975  E625 C7CD A89B 7B53 E8B6

To claim this, I am signing this object:

```json
{
  "body": {
    "key": {
      "eldest_kid": "010140deb6f53ceb9b1e5c9c14b5cb364d033c57e8d60adaaf2aee23d88956357ae50a",
      "fingerprint": "75cf6c8f834965036975e625c7cda89b7b53e8b6",
      "host": "keybase.io",
      "key_id": "c7cda89b7b53e8b6",
      "kid": "010140deb6f53ceb9b1e5c9c14b5cb364d033c57e8d60adaaf2aee23d88956357ae50a",
      "uid": "c497eb586d68eb31da382a66c2cc8b19",
      "username": "puckipedia"
    },
    "service": {
      "name": "github",
      "username": "puckipedia"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "ctime": 1501591828,
  "expire_in": 157680000,
  "prev": "0eae34a91ef0f7860d4c5ca5e9ce18358e378309fbfb2e524b7b0bf61e4e891d",
  "seqno": 11,
  "tag": "signature"
}
```

with the key [75CF 6C8F 8349 6503 6975  E625 C7CD A89B 7B53 E8B6](https://keybase.io/puckipedia), yielding the signature:

```
-----BEGIN PGP MESSAGE-----
Version: GnuPG v2

owGtUntQVFUc3gVCdChRKiIbi4uQKMG9e1/ngo0Y5USakcYUDrTdc+65y+Wxu+4z
XAmGh8YzpCwpUcEZchUdsdECBKUsnIwBK2QzHjZFLs2gifiIFLpr9l/TX51/zpzv
fN93vvPNr/Z+f02Qtqd3/0euDd7j2rPjUJNelJ/iIqBJyicSXEQOvrvhXAlbbfoc
RSISCJIiKYaUMORklkYYCpDCLBIQxUAWQZpjJJKmEctjIHGkKImirBMx1tESAALL
0SwvYpYUiVhCVowGbDFbFKNNteVZJHMIyIBmBI4laU7gWczpWMQjSQQC5CFLYwA5
VZhlsvoUajgoWnGcYlIx9aC/G+9f+P9zbvvf7zACjyELOIkDGNKUJNJAJ3Ic0iEE
ICX4iFZsMYp5WGWb7ShHMWNJEYmCWELFHQrCvmrv3RsUW5Yd/pfGlm/2gU4M9ffk
eqgYJbVDVeXAFqtiMhIJlMpENsWnp1iSYgUK6EAsgd80KxasV3wMlucAqa5YwmzB
Dl8xWMQ0IwoUltNImQccKTGIRSKLBYQpQLMA0zygSUGGMtRhVseo5ZJQ5ijMYCBQ
EuH70SajSTWn1KCiQTW1KgajaLNbMFEwr9zvsQCNNkgTeJ+fb7w08+aG/DNzSy7O
n13z4r5tbW23PBT65qstXSC1IzN199EvR6KeKK7MfM5elta3bt+q4IypmgBz4qsB
zLUmb8fUj453wO0Bv3BP9c36qLSC8KRlv769MTIb8U+Nuj9svRITlMEdc+d++8mZ
j3dgc9WhAkPy5YV7Hu+y5jXFwOiqwOMxa/SrvV2LPbuc73d+X9jQE1Sys/ePwhN1
+c03KjMGzl13DM0Uh3fPCbk+FtZ8YHp0RUn/rQDH3h8+3emcmX3g+ad/imxbbpnY
UBF8Z/pa79pk02uLzpTemKw/5to60RswcSddLl08s9yfe2OXZ2Amt6Nn+9dHqke2
jq8crCmJm32rM2N9WLUh0Xtk3G2vvDRUuODZZ2qOhhb5GVvch3c48yrOZmsH92Y+
0hMM5ljSy77gwkpA6tX0htDyqpYHVybtnhsPwvtPE8six4oujRwcRo2na4tPRa3/
rtHw88HWwerZiJT8Pv8lwxfau2tnL/QV/vLBQz3bl64z3ZwoeqlFczJ6gX3LZ+8t
Yjbdbl0Y13++d3VyzPmXH+VOlSd9vjGK2ez2XNZvjk8KnGTGTFOh+1+QvJ6Ipa6w
ww9LrjGhzsETJYYpT3hpY2e3pzZ4tCKtsu735vqm9gPvOoeTXyGvOEKeJLJOHEot
w53T+qGL59pLrX8WGSPQ1YHE+mb3/BUpTPaekzPx5te9tVopeRKvXfWbviH6Lw==
=33Vx
-----END PGP MESSAGE-----

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/puckipedia

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id puckipedia
```
